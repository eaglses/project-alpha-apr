from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


# list view projects
@login_required(redirect_field_name="/accounts/login/")
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects_objects": projects}
    return render(request, "projects/list.html", context)


# detailed list project
@login_required(redirect_field_name="/accounts/login/")
def project_detail(request, id):
    details = Project.objects.get(id=id)
    context = {
        "list_object": details,
    }
    return render(request, "projects/detailed.html", context)


# create new project
@login_required(redirect_field_name="/accounts/login/")
def creat_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            pform = form.save(False)

            pform.author = request.user
            pform.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

        context = {
            "form": form,
        }
    return render(request, "projects/create.html", context)

