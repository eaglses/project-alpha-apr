from django.urls import path
from tasks.views import create_task, users_task_list, edit_task, create_task_note


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", users_task_list, name="show_my_tasks"),
    path("<int:id>/edit/", edit_task, name="edit_task"),
    path("<int:id>/addnote", create_task_note, name="create_note"),
]
