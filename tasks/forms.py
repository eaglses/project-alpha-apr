from django.forms import ModelForm
from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]


class TaskCompleteForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "is_completed"
        ]


class createNoteForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "notes",
        ]
