from django.shortcuts import render, redirect, get_object_or_404
from tasks.models import Task
from tasks.forms import TaskForm, TaskCompleteForm, createNoteForm
from django.contrib.auth.decorators import login_required


# create task
@login_required(redirect_field_name="/accounts/login/")
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            pform = form.save()

            pform.author = request.user
            pform.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

        context = {
            "form": form,
        }
    return render(request, "tasks/create.html", context)


# filter list to user
@login_required(redirect_field_name="/accounts/login/")
def users_task_list(request):
    tasks_objects = Task.objects.filter(assignee=request.user)
    context = {"tasks_objects": tasks_objects}
    return render(request, "tasks/list.html", context)


# edit the tasks of a project 
@login_required(redirect_field_name="/accounts/login/")
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskCompleteForm(request.POST, instance = task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskCompleteForm(instance = task)

        context = {
        "task_object" : task,
        "form" : form
    }
    return render(request, "tasks/create.html", context)

#add note to the tasks
@login_required(redirect_field_name="/accounts/login/")
def create_task_note(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = createNoteForm(request.POST, instance = task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = createNoteForm(instance = task)

        context = {
        "task_object": task,
        "form": form
    }
    return render(request, "tasks/create.html", context)
